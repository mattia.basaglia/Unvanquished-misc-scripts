#
# Copyright (C) 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import socket
import re
import sys

HEADER = "\xff\xff\xff\xff"  # OOB packet header


class ErrorPolicy:
    """
    Policy on how to handle errors
    """
    Ignore = 0
    Print = 1
    Throw = 2

    def __init__(self, policy):
        self.policy = policy

    def error(self, message):
        if self.policy == 1:
            print >>sys.stderr, message
        elif self.policy == 2:
            raise OOBError(message)

    @staticmethod
    def default():
        return ErrorPolicy(ErrorPolicy.Ignore)


class OOBError(Exception):
    """
    Exception raised by ErrorPolicy (when set to throw)
    """
    pass


def udp_send(server, payload):
    """
    Sends a UDP packet and returns the socket
    """
    sock = socket.socket(server.socket_address_type(), socket.SOCK_DGRAM)
    sock.settimeout(1)
    sock.sendto(payload, server.socket_tuple())
    return sock


def udp(server, payload, readsize=32768, policy=ErrorPolicy.default()):
    """
    Sends a UDP packet and returns the reply
    """

    try:
        return udp_send(server, payload).recvfrom(readsize)[0]
    except Exception, e:
        policy.error("Connection to %s failed: %s" % (server, e))
        return ""


def oob_send(server, command):
    """
    Sends an OOB command and returns the socket
    """
    return udp_send(server, HEADER + command)


def oob(server, command, policy=ErrorPolicy.default()):
    """
    Sends a OOB command and returns the reply
    """
    result = udp(server, HEADER + command, policy=policy)
    if result.startswith(HEADER):
        return result[len(HEADER):]
    return result


class Host(object):
    """
    Host/port pair
    """

    def __init__(self, *args, **kwargs):
        if len(args) == 2:
            self.host = str(args[0])
            self.port = int(args[1])
        elif len(args) == 1:
            self.parse(args[0], kwargs.get("default_port", None))
        else:
            raise Exception("Invalid arguments for Host")

        if "ip_version" in kwargs:
            self.ip_version = kwargs["ip_version"]
        elif ":" in self.host:
            self.ip_version = 6
        else:
            self.ip_version = 4

    def parse(self, string, default_port):
        """
        Parses a string in the form host:port
        """
        match = re.match("([^:]+|\[[^\]]+\])(?::([0-9]+))?$", string)
        if not match:
            raise Exception("Wrong host format")
        self.host = match.group(1)
        if match.group(2):
            self.port = int(match.group(2))
        elif default_port is None:
            raise Exception("Missing port number")
        else:
            self.port = default_port

    def __str__(self):
        return "%s:%d" % (self.host, self.port)

    def socket_tuple(self):
        """
        Returns a tuple suitable to open a socket
        """
        if self.ip_version == 6:
            return (self.host.strip("[]"), self.port, 0, 0)
        else:
            return (self.host, self.port)

    def socket_address_type(self):
        if self.ip_version == 6:
            return socket.AF_INET6
        return socket.AF_INET

    def __eq__(self, other):
        return self.host == other.host and self.port == other.port

    def __ne__(self, other):
        return not (self == other)
