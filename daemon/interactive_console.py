import os
import readline
import argparse
import sys
import keyword
import traceback
import __builtin__
import __main__


class CommandBase(object):
    """
    Base class for commands
    """

    def completions(self, prefix, controller):
        """
        Returns a list of completions for the given prefix
        """
        return []

    def run(self, arguments, controller):
        """
        Must be implemented with the proper behaviour
        """
        raise NotImplementedError("Unimplemented")

    def trigger(self):
        """
        Returns the trigger for this command
        """
        return self._get_trigger()

    def _get_trigger(self):
        """
        Must be implemented and return a string used to identify the command
        """
        raise NotImplementedError("Unimplemented")

    def help(self):
        """
        Prints a help message
        """
        pass


class FixedTriggerCommand(CommandBase):
    """
    Base class for commands with a fixed trigger
    """
    def __init__(self, trigger):
        if not str(trigger):
            raise ValueError("Invalid trigger")
        self._trigger = str(trigger).lower()

    def _get_trigger(self):
        return self._trigger


class LambdaCommand(FixedTriggerCommand):
    """
    Command that executes a simple function
    """
    def __init__(self, trigger, function):
        super(LambdaCommand, self).__init__(trigger)
        self.function = function

    def run(self, arguments, controller):
        self.function()


class Property(object):
    """
    Controller property
    """
    # Property flags:
    HIDDEN    = 0  # Only accessible by code
    READABLE  = 1  # Can be read with get
    EDITABLE  = 2  # Can be altered with set
    READWRITE = 3  # Ban be read and written using get/set

    def __init__(self, name, help, default=None,
                 flags=READABLE, type=lambda x: x):
        self.name = name
        self.help = help
        self._value = default
        self.flags = flags
        self.type = type

    def get(self, controller):
        """
        Returns the property value
        """
        return self._value

    def set(self, value, controller):
        """
        Sets the property value
        """
        self._value = self.type(value)


class LazyProperty(Property):
    """
    Controller property with lazy initialization
    """
    def __init__(self, name, help, initializer, *args, **kwargs):
        super(LazyProperty, self).__init__(name, help, None, *args, **kwargs)
        self._initializer = initializer
        self._initialized = False

    def get(self, controller):
        """
        Lazy initialization
        """
        if not self._initialized:
            self._value = self._initializer()
            self._initialized = True

        return self._value


class History(object):
    """
    Object that handles a readline history context
    """

    def __init__(self, history_file):
        self.history_file = history_file

    def __enter__(self):
        """
        Sets up readline
        """
        history_dir = os.path.dirname(self.history_file)
        if not os.path.exists(history_dir):
            os.makedirs(history_dir)

        try:
            readline.read_history_file(self.history_file)
            readline.set_history_length(1024)
        except IOError:
            pass

    def __exit__(self, *args):
        """
        Finalizes readline history
        """
        readline.write_history_file(self.history_file)


class Controller(object):
    """
    Controller object, manages commands and user input
    """

    @staticmethod
    def handler_raise(exception):
        """
        Exception handler that keeps the exception
        """
        raise

    @staticmethod
    def handler_ignore(exception):
        """
        Exception handler that ignores the exception
        """
        pass

    @staticmethod
    def handler_print(exception):
        """
        Exception handler that prints the exception and keeps going
        """
        print "Error:", exception

    @staticmethod
    def handler_backtrace(exception):
        """
        Exception handler that prints the exception and keeps going
        """
        traceback.print_exc()

    def __init__(self, quit_message=""):
        self.commands = []
        self.prompt = "> "
        self._matches = []
        self.quit_message = quit_message
        self.properties = {}

        readline.set_completer(self._completer)
        readline.parse_and_bind("tab: complete")

    def _completer(self, text, state):
        """
        Readline autocompletion function
        """
        if state == 0:
            context = readline.get_line_buffer()
            if len(context) > len(text):
                command = self.get_command(self._split_command_line(context)[0])
                if command:
                    self._matches = command.completions(text, self)
                else:
                    self._matches = []
            else:
                self._matches = [
                    command.trigger()
                    for command in self.commands
                    if command.trigger().startswith(text)
                ]

            if len(self._matches) == 1:
                return self._matches[0] + " "

        if state < len(self._matches):
            return self._matches[state]

        return None

    def add_command(self, command):
        """
        Adds a command to the controller
        """
        if not isinstance(command, CommandBase):
            raise TypeError("Expected CommandBase object")

        self.commands.append(command)

    def run_line(self, line):
        """
        Runs the appropriate command
        """
        command_name, args = self._split_command_line(line)
        if not command_name:
            return

        command = self.get_command(command_name)
        if command:
            command.run(args, self)

    def _split_command_line(self, line):
        """
        Splits a string into the command name and the argument string
        """
        array = line.strip().split(None, 1)
        if len(array) == 1:
            return (array[0].lower(), "")
        elif len(array) == 2:
            return (array[0].lower(), array[1])
        return ("", "")

    def get_command(self, trigger):
        """
        Returns the command matching the trigger, None if there is no match
        """
        for command in self.commands:
            if command.trigger() == trigger:
                return command
        return None

    def run(self, exception_handler=handler_raise):
        """
        Loops until quit or end of input
        """
        if "__func__" in dir(exception_handler):
            exception_handler = exception_handler.__func__
        while True:
            try:
                self.run_line(raw_input(self.prompt))
            except EOFError:
                print self.quit_message
                break
            except KeyboardInterrupt:
                print self.quit_message
                break
            except Exception as e:
                exception_handler(e)

    def run_file(self, filename, exception_handler=handler_raise):
        """
        Runs all of the lines in the file
        """
        if "__func__" in dir(exception_handler):
            exception_handler = exception_handler.__func__
        if os.path.exists(filename):
            with open(filename) as file:
                for line in file:
                    try:
                        self.run_line(line)
                    except Exception as e:
                        exception_handler(e)

    def add_property(self, property):
        """
        Adds a property to the controller
        """
        if not isinstance(property, Property):
            raise TypeError("Expected Property object")

        if property.name in self.properties:
            raise KeyError("Property '%s' is already present" % property.name)

        self.properties[property.name] = property

    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__["name"]
        if "properties" not in self.__dict__:
            return None
        if name in self.properties:
            return self.properties[name].get(self)
        return None

    def __setattr__(self, name, value):
        if name in self.__dict__ or "properties" not in self.__dict__:
            self.__dict__[name] = value
        elif name in self.properties:
            self.properties[name].set(value, self)
        else:
            self.add_property(self,
                Property(name, "User-defined property", value, True)
            )


class Quit(FixedTriggerCommand):
    """
    Quits the current process
    """
    def __init__(self):
        super(Quit, self).__init__("quit")

    def run(self, args, controller):
        sys.exit()

    def help(self):
        print "Exits the console."


class Help(FixedTriggerCommand):
    """
    Shows help for commands and properties
    """
    def __init__(self, trigger="help"):
        super(Help, self).__init__(trigger)

    def run(self, args, controller):
        print ""
        search = args
        if search:
            for command in controller.commands:
                if command.trigger() == search:
                    self.print_title(search + " (command)")
                    print ""
                    command.help()
                    print ""
                    return
            if search in controller.properties:
                property = controller.properties[search]
                if property.flags:
                    if property.flags & Property.EDITABLE:
                        read_only = ""
                    else:
                        read_only = "read only "
                    self.print_title(search + "(%sproperty)" % read_only)
                    print ""
                    print "Value: %s" % property.get(controller)
                    print ""
                    print property.help
                    print ""
                    return

            print "Term not found: %s\n" % search

        self.print_section(
            "Commands",
            sorted(command.trigger() for command in controller.commands)
        )

        self.print_section(
            "Properties",
            sorted(
                property.name
                for property in controller.properties.values()
                if property.flags
            )
        )

    def help(self):
        print self.trigger() + " [term]\n"
        print "Shows this help."

    def print_section(self, title, items):
        if items:
            self.print_title(title)
            print ""
            self.print_columns(items)
            print ""

    def print_title(self, title):
            print title
            print "-" * len(title)

    def print_columns(self, list, max_width=80):
        if not list:
            return

        column_separator = "  "

        # Start from a single row, then increase until we can fit all the elements
        for row_count in range(1, len(list)):
            # Split the main list into columns
            chunks = [list[i:i+row_count] for i in xrange(0, len(list), row_count)]
            # Compute the maximum length of each column
            colmn_widths = [max(len(item) for item in chunk) for chunk in chunks]
            # Check if the total with with this arrangement is suitable
            total_width = sum(colmn_widths) + len(column_separator) * (len(chunks) - 1)
            if total_width <= max_width:
                break
        else:
            # Not enough room, print every item on its own line
            chunks = [len(list)]
            colmn_widths = [0]

        # Ensure the last chunk has enough elements
        if len(chunks) > 1:
            chunks[-1] += [""] * (len(chunks[0]) - len(chunks[-1]))

        for row in zip(*chunks):
            print column_separator.join(
                item.ljust(colmn_widths[index])
                for index, item in enumerate(row)
            )


class Set(FixedTriggerCommand):
    """
    Sets a controller property
    """
    def __init__(self, interpret_python=False, trigger="set", unset=None):
        super(Set, self).__init__(trigger)
        self.interpret_python = interpret_python
        self.unset = unset

    def run(self, args, controller):
        args = args.split(None, 1)
        if len(args) == 1:
            property = args[0]
            value = self.unset
        else:
            property, value = args
            if self.interpret_python:
                try:
                    value = eval(value)
                except Exception as exc:
                    print "Invalid expression: %s" % exc
                    return
        if property in controller.properties and \
            (controller.properties[property].flags & Property.EDITABLE):
                controller.properties[property].set(value, controller)

    def completions(self, prefix, controller):
        return [
            property.name
            for property in controller.properties.values()
            if property.name.startswith(prefix)
            and (property.flags & Property.EDITABLE)
        ]

    def help(self):
        print "Sets a property of the console."
        if self.interpret_python:
            print "The value is interpreted as python code."


class Get(FixedTriggerCommand):
    """
    Gets a controller property
    """
    def __init__(self, trigger="get"):
        super(Get, self).__init__(trigger)

    def run(self, args, controller):
        if args in controller.properties.keys() and \
            (controller.properties[args].flags & Property.READABLE):
                print controller.properties[args].get(controller)

    def completions(self, prefix, controller):
        return [
            property.name
            for property in controller.properties.values()
            if property.name.startswith(prefix)
            and (property.flags & Property.READABLE)
        ]

    def help(self):
        print "Prints the value of a property of the console."


class ArgumentError(Exception):
    """
    Error raised by SaneArgumentParser
    """
    pass


class SaneArgumentParser(argparse.ArgumentParser):
    """
    Why would anyone call sys.exit() on error?
    """
    def error(self, message):
        raise ArgumentError(message)

    def exit(self, status, message):
        pass


class Eval(FixedTriggerCommand):
    """
    Evaluates some python code
    """
    def __init__(self, print_result=True, trigger="eval"):
        super(Eval, self).__init__(trigger)
        self.print_result = print_result
        self.completer = PythonCompleter()

    def run(self, args, controller):
        try:
            value = eval(args, __main__.__dict__)
            if self.print_result:
                print value
        except Exception as exc:
            print "Invalid expression: %s" % exc
            return

    def completions(self, prefix, controller):
        return self.completer.completions(prefix)

    def help(self):
        print "Evaluates some python code."


class Echo(FixedTriggerCommand):
    """
    Prints a fixed string
    """
    def __init__(self, trigger="echo"):
        super(Echo, self).__init__(trigger)

    def run(self, args, controller):
        print args

    def help(self):
        print self.__doc__.strip()


class PythonCompleter(object):
    """
    Object that provides simple autocompletion facilities for python code
    """

    def completions(self, prefix):
        """
        Gives a list of possible completions from that prefix
        """
        if not prefix:
            return []

        path = prefix.split('.')
        vars = dict(__builtin__.__dict__.items() + __main__.__dict__.items())
        if len(path) == 1:
            completions = self.completions_for_object(path[0], vars) + \
                [kw for kw in keyword.kwlist if kw.startswith(path[0])]
        else:
            object = self.find_object(path[:-1], vars)
            if object is None:
                return []
            completions = self.completions_for_object(path[-1], object.__dict__)

        if len(path) > 1:
            compl_prefix = ".".join(path[:-1]) + "."
        else:
            compl_prefix = ""
        return [compl_prefix + compl for compl in completions]

    def find_object(self, path, namespace):
        """
        Find an object following a list of string
        """
        if not path or path[0] not in namespace:
            return None
        if len(path) > 1:
            return self.find_object(path[:-1], namespace[path[0]].__dict__)
        return namespace[path[0]]

    def completions_for_object(self, prefix, namespace):
        """
        Returns a list of viable completions for an object
        """
        return [
            key
            for key in namespace.keys()
            if key.startswith(prefix) and not key.startswith("_")
        ]

    def completer(self, text, state):
        """
        Readline autocompletion funtion
        """
        if state == 0:
            self._matches = self.completions(text)

        if len(self._matches) == 1:
            return self._matches[0] + " "

        if state < len(self._matches):
            return self._matches[state]

        return None
