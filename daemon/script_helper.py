#
# Copyright (C) 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
from network import Host
from .string import String


class HostArg:
    """
    Argument type for servers (host:port)
    """
    def __init__(self, default_port=None):
        self.default_port = default_port

    def __call__(self, arg):
        try:
            return Host(arg, default_port=self.default_port)
        except Exception, e:
            raise argparse.ArgumentTypeError(e.message)


def print_info(info_map, keys=[], string_colors=String.COLOR_ANSI):
    """
    Formats and prints a server info map
    """
    check_key = lambda key: not keys or key in keys
    max_key = max(len(key) for key in info_map.keys() if check_key(key))
    for key, val in info_map.iteritems():
        if check_key(key):
            print("\x1b[1m%s\x1b[0m %s" % (
                key.ljust(max_key, ' '),
                String(val).to_string(string_colors)
            ))


def print_players(players, string_colors=String.COLOR_ANSI):
    """
    Formats and prints a player list
    """
    print("\x1b[1m score   ping name\x1b[0m")
    for player in players:
        print("%6s %6s %s" % (
            player.score,
            player.ping,
            player.name.to_string(string_colors)
        ))
