#
# Copyright (C) 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


class Color:
    """
    24 bit RGB color
    """

    def __init__(self, *args, **kwargs):
        self.valid = False

        if len(args) == 3:
            self.red, self.green, self.blue = args
            if "float" in kwargs and kwargs["float"]:
                self.red = int(round(self.red * 255))
                self.green = int(round(self.green * 255))
                self.blue = int(round(self.blue * 255))
            self.valid = True

    def ansi(self, standard=True):
        """
        Returns an ansi sequence that encodes the color
        """

        if not self.valid:
            return "\x1b[0m"

        if not standard:
            return "\x1b[38;2;%d;%d;%dm" % (self.red, self.green, self.blue)

        color = 0
        red = self.red / 255.0
        green = self.green / 255.0
        blue = self.blue / 255.0

        cmax = max(red, green, blue)
        cmin = min(red, green, blue)
        delta = cmax-cmin

        if delta > 0:
            hue = 0
            if red == cmax:
                hue = (green-blue)/delta
            elif green == cmax:
                hue = (blue-red)/delta + 2
            elif blue == cmax:
                hue = (red-green)/delta + 4

            sat = delta / cmax
            if sat >= 0.3:
                if hue < 0:
                    hue += 6

                if hue <= 0.5:
                    color = 1  # red
                elif hue <= 1.5:
                    color = 3  # yellow
                elif hue <= 2.5:
                    color = 2  # green
                elif hue <= 3.5:
                    color = 6  # cyan
                elif hue <= 4.5:
                    color = 4  # blue
                elif hue <= 5.5:
                    color = 5  # magenta
                else:
                    color = 1  # red
            elif cmax >= 0.5:
                color = 7

            bright = (cmax + cmin) / 2 >= 0.64
        else:
            if cmax > 0.8:
                color = 7  # white
                bright = True
            elif cmax > 0.53:
                color = 7  # silver
                bright = False
            elif cmax > 0.27:
                color = 0  # gray
                bright = True
            else:
                color = 0  # black
                bright = False

        return "\x1b[%d;3%dm" % (1 if bright else 22, color)


# Taken from Color.cpp, replacing
# \s+\{ ([0-9.]+)f, ([0-9.]+)f, ([0-9.]+)f, 1.00f \}, // (.).*
# with
#     '\4': Color(\1, \2, \3, float=True),
DAEMON_COLORS = {
    '0': Color(0.20, 0.20, 0.20, float=True),
    '1': Color(1.00, 0.00, 0.00, float=True),
    '2': Color(0.00, 1.00, 0.00, float=True),
    '3': Color(1.00, 1.00, 0.00, float=True),
    '4': Color(0.00, 0.00, 1.00, float=True),
    '5': Color(0.00, 1.00, 1.00, float=True),
    '6': Color(1.00, 0.00, 1.00, float=True),
    '7': Color(1.00, 1.00, 1.00, float=True),
    '8': Color(1.00, 0.50, 0.00, float=True),
    '9': Color(0.50, 0.50, 0.50, float=True),
    ':': Color(0.75, 0.75, 0.75, float=True),
    ';': Color(0.75, 0.75, 0.75, float=True),
    '<': Color(0.00, 0.50, 0.00, float=True),
    '=': Color(0.50, 0.50, 0.00, float=True),
    '>': Color(0.00, 0.00, 0.50, float=True),
    '?': Color(0.50, 0.00, 0.00, float=True),
    '@': Color(0.50, 0.25, 0.00, float=True),
    'A': Color(1.00, 0.60, 0.10, float=True),
    'B': Color(0.00, 0.50, 0.50, float=True),
    'C': Color(0.50, 0.00, 0.50, float=True),
    'D': Color(0.00, 0.50, 1.00, float=True),
    'E': Color(0.50, 0.00, 1.00, float=True),
    'F': Color(0.20, 0.60, 0.80, float=True),
    'G': Color(0.80, 1.00, 0.80, float=True),
    'H': Color(0.00, 0.40, 0.20, float=True),
    'I': Color(1.00, 0.00, 0.20, float=True),
    'J': Color(0.70, 0.10, 0.10, float=True),
    'K': Color(0.60, 0.20, 0.00, float=True),
    'L': Color(0.80, 0.60, 0.20, float=True),
    'M': Color(0.60, 0.60, 0.20, float=True),
    'N': Color(1.00, 1.00, 0.75, float=True),
    'O': Color(1.00, 1.00, 0.50, float=True),
}
