#
# Copyright (C) 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from color import Color, DAEMON_COLORS


class String(object):
    COLOR_REENCODE      = -1 # Re-encode colors to their original form
    COLOR_STRIP         = 0  # Strip colors, keep only the text
    COLOR_ANSI          = 1  # Encode colors to standard ansi
    COLOR_ANSI_EXTENDED = 2  # Encode colors to 24bit ansi codes

    """
    String with sequences of colored text
    """
    def __init__(self, input, default_color=Color()):
        """
        Parses the input
        """
        if isinstance(input, String):
            self._raw = input._raw
            self._string = input._string
        else:
            self._raw = str(input)
            self._string = _Parser(default_color).parse(self._raw)
            self._string += [(default_color, "")]

    def raw(self):
        """
        Returns the raw string that was parsed to get this object
        """
        return self._raw

    def plain_text(self):
        """
        Returns the string without color codes
        """
        return "".join(item[1] for item in self._string)

    def ansi(self, standard=True):
        """
        Returns the string with ANSI-encoded colors
        """
        ansi = ""
        for element in self._string:
            ansi += element[0].ansi(standard) + element[1]
        return ansi

    def to_string(self, mode):
        if mode == String.COLOR_REENCODE:
            return self._raw
        elif mode == String.COLOR_STRIP:
            return self.plain_text()
        elif mode == String.COLOR_ANSI:
            return self.ansi(True)
        elif mode == String.COLOR_ANSI_EXTENDED:
            return self.ansi(False)
        raise Exception("Unrecognize color mode")


class _Parser(object):
    """
    Parser that splits a Daemon colored string into chunks of similar colors
    """

    def __init__(self, default_color=Color):
        self.default_color = default_color

    def _push_string(self):
        if self._current_string:
            self._parsed += [(self._current_color, self._current_string)]
            self._current_string = ""

    def _push_color(self, color):
        self._push_string()
        self._current_color = color

    def _lex_caret(self, i):
        if i + 1 >= len(self._subject):
            self._current_string += '^'
            return i+1
        if self._subject[i+1] == 'x':
            r = int(self._subject[i+2], 16)
            g = int(self._subject[i+3], 16)
            b = int(self._subject[i+4], 16)
            self._push_color(Color(r * 0x11, g * 0x11, b * 0x11))
            return i+5
        elif self._subject[i+1] == '#':
            r = int(self._subject[i+2:4], 16)
            g = int(self._subject[i+4:6], 16)
            b = int(self._subject[i+6:8], 16)
            self._push_color(Color(r, g, b))
            return i+8
        elif self._subject[i+1] == '^':
            self._current_string += '^'
            return i+2
        elif self._subject[i+1] == '*':
            self._push_color(self.default_color)
            return i+2
        elif self._subject[i+1].upper() in DAEMON_COLORS:
            self._push_color(DAEMON_COLORS[self._subject[i+1].upper()])
            return i+2
        else:
            self._current_string += '^'
            return i+1

    def parse(self, string):
        if not string:
            return []

        self._subject = string
        self._parsed = []
        self._current_string = ""
        self._current_color = self.default_color

        i = 0
        while i < len(string):
            if string[i] == '^':
                i = self._lex_caret(i)
            else:
                self._current_string += string[i]
                i += 1

        self._push_string()

        return self._parsed
