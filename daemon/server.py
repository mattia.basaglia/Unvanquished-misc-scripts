#
# Copyright (C) 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re
import StringIO
from network import oob, oob_send, Host, ErrorPolicy
from .string import String


def parse_info_string(string):
    """
    Parses an info string into a dictionary
    """
    if not string:
        return {}
    flat = string.split("\\")
    start = 1 if string[0] == '\\' else 0
    return dict(zip(flat[start::2], flat[start+1::2]))


class Player(object):
    """
    A player in the server
    """
    def __init__(self, name, ping=0, score=0):
        self.name = String(name)
        self.ping = ping
        self.score = score


class Server(object):
    """
    A Daemon Server
    """
    DEFAULT_PORT = 27960

    def __init__(self, host, rcon_password=""):
        self.host = host
        self.rcon_password = rcon_password
        self._status = None
        self._players = None
        self._rcon_secure = None

    def oob(self, command, policy=ErrorPolicy.default()):
        """
        Runs an out of bound command and returns the result as a list of lines
        """
        return oob(self.host, command, policy=policy).rstrip("\n").split("\n")

    def __getattr__(self, name):
        if name == "status":
            if self._status is None:
                self.get_status()
            return self._status
        if name == "players":
            if self._players is None:
                self.get_status()
            return self._players
        if name == "rcon_secure":
            self.ensure_rcon_info()
            return self._rcon_secure

    def get_status(self):
        """
        Updates the status information
        """
        self._parse_status(self.oob("getstatus"))
        return (self._status, self._players)

    def challenge(self, policy=ErrorPolicy.default()):
        """
        Sends a challenge and returns it as a string
        """
        oob = self.oob("getchallenge")
        challenge_header = "challengeResponse "
        if oob and challenge_header in oob[0]:
            return oob[0][len(challenge_header):]
        policy.error("No challenge response by the server")
        return ""

    def _parse_status(self, status_response):
        """
        Parses a statusResponse into a pair with
        the server features as a dictionary and players as a list
        """
        if len(status_response) < 2:
            self._status = {}
            self._players = []
            return

        self._status = parse_info_string(status_response[1])
        for key, val in self._status.iteritems():
            self._status[key] = String(val)

        self._players = []
        for player in status_response[2:]:
            match = re.match(r'(\d+)\s+(\d+)\s+"(.*)"$', player)
            if match:
                self._players.append(
                    Player(match.group(3), match.group(2), match.group(1))
                )

    def get_rcon_info(self):
        """
        Updates rcon information
        """
        info_string = self.oob("rconinfo")
        if len(info_string) == 2:
            info = parse_info_string(info_string[1])
            if "secure" in info:
                self._rcon_secure = int(info["secure"])
            return info
        return {}

    def ensure_rcon_info(self):
        """
        Ensures rcon info is collected, without sending any messages if
        it all the information is already available
        """
        if self._rcon_secure is None:
            self.get_rcon_info()

    def rcon(self, command):
        """
        Executes a rcon command
        """
        if self.rcon_secure == 0:
            return self.oob("rcon %s %s" % (self.rcon_password, command))[1:]

    def __str__(self):
        return str(self.host)


class MasterServer():
    """
    Daemon master server
    """

    DEFAULT_HOST = "master.unvanquished.net"
    DEFAULT_PORT = 27950
    GAMES = ["UNVANQUISHED"]
    GAME_PROTOCOL = 86

    def __init__(self, host=Host(DEFAULT_HOST, DEFAULT_PORT),
                 game=GAMES[0], protocol=GAME_PROTOCOL):
        self.host = host
        self._servers = None
        self.game = game
        self.protocol = protocol

    def __getattr__(self, name):
        if name == "servers":
            if self._servers is None:
                self.get_servers()
            return self._servers

    def get_servers(self, extra_flags="empty full",
                    policy=ErrorPolicy.default()):
        """
        Updates the server list
        """
        self._servers = []
        payload = "getserversExt %s %i %s" % (
            self.game, self.protocol, extra_flags
        )

        packet_index = 0
        packet_count = 1
        try:
            sock = oob_send(self.host, payload)
            readsize = 1024

            while packet_index < packet_count:
                (packet_index, packet_count) = \
                    self._parse_server_list(sock.recvfrom(readsize)[0])
        except Exception, e:
            policy.error(
                "Connection #%d to %s failed: %s" %
                (packet_index + 1, self.host, e)
            )
        return self._servers

    def _parse_server_list(self, datagram):
        """
        Parses the result of a getserversExt response from the master.
        Returns a pair with the packet index and the total number of packets
        """

        buffer = StringIO.StringIO(datagram)

        def nextbyte():
            return buffer.read(1)

        def read_until(skipped):
            byte = nextbyte()
            read = ""
            while byte not in skipped and byte != "":
                read += byte
                byte = nextbyte()
            return (byte, read)

        def skip(skipped="\\/"):
            return read_until(skipped)[0]

        byte = skip("\\/\0")

        packet_index = 1
        packet_count = 1
        if byte == '\0':
            byte, packet_index = read_until("\\/\0")
            if byte == '\0':
                byte, packet_count = read_until("\\/\0")
            if byte not in "\\/":
                byte = skip()

        try:
            while byte != "":
                if byte == "\\": # IPv4
                    ip = []
                    for i in range(0, 4):
                        ip += [str(ord(nextbyte()))]
                    port = ord(nextbyte()) << 8
                    port |= ord(nextbyte())
                    self._servers.append(Server(Host(".".join(ip), port)))
                elif byte == "/": # IPv6
                    ip = []
                    for i in range(0, 8):
                        high = format(ord(nextbyte()), '02x')
                        low = format(ord(nextbyte()), '02x')
                        ip += [high+low]
                    port = ord(nextbyte()) << 8
                    port |= ord(nextbyte())
                    self._servers.append(Server(
                        Host("["+":".join(ip)+"]", port, ip_version=6)
                    ))

                byte = nextbyte()
        except:
            pass

        return (packet_index, packet_count)

    def __str__(self):
        return str(self.host)


class ServerRegistry(object):
    """
    Class used to register and retrieve server objects whose
    lifetime will persist at least for as long as the lifetime of the registry
    """
    def __init__(self, *args):
        self._servers = []
        self._masters = []
        for arg in args:
            if isinstance(arg, MasterServer):
                self.add_master(arg)
            else:
                self.add_server(arg)

    def add_server(self, server):
        """
        Adds a server to the list of registered servers,
        if this doesn't already exists.
        server can be a Server or a Host object.
        """

        return self.get_server(server)

    def add_master(self, master):
        """
        Adds a master server, wrapping get_servers() in a way that ensures
        all servers are registered
        """
        for ms in self._masters:
            if master is ms:
                return ms

        get_servers = master.get_servers

        def wrapped_get_servers(*args, **kwargs):
            get_servers(*args, **kwargs)
            for server in master._servers:
                self.add_server(server)
        master.get_servers = wrapped_get_servers

        self._servers.append(master)
        return master

    def get_server(self, server):
        """
        Returns a server corresponding to the one passed as a parameter.
        If the server doesn't exist, it's added to the registry
        """

        if isinstance(server, Host):
            server = Server(server)
        elif type(server) is str:
            server = Server(Host(server, default_port=Server.DEFAULT_PORT))
        elif not isinstance(server, Server):
            raise Exception("Unrecognized server object")

        for sv in self._servers:
            if sv.host == server.host:
                return sv

        self._servers.append(server)

        return server

    def __getitem__(self, key):
        return self.get_server(key)
