# Scripts related to Unvanquished/Daemon

## Description

Here are some scripts (and related libraries) designed to communicate with Unvanquished servers.

* **master** - Can list the servers available to the Daemon master, and their status
* **rcon** - Can send rcon commands (or other out of band messages)
* **console** - A console interface with the functionality of the other scripts

## License

GPLv3+, see COPYING

## Sources

https://gitlab.com/mattia.basaglia/Unvanquished-misc-scripts

## Author

Mattia "Melanosuchus" Basaglia
